# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Robert Antoni Buj i Gelonch <rbuj@fedoraproject.org>, 2015-2016
# Richard Hughes <richard@hughsie.com>, 2016. #zanata
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016. #zanata
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: appstream-glib 0.6.7\n"
"Report-Msgid-Bugs-To: richard@hughsie.com\n"
"POT-Creation-Date: 2017-01-12 10:54+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-02-22 02:11-0500\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan (http://www.transifex.com/freedesktop/appstream-glib/"
"language/ca/)\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.9.6\n"

#. TRANSLATORS: command line option
#: client/as-builder.c:90 client/as-compose.c:340 client/as-util.c:4173
msgid "Show extra debugging information"
msgstr "Mostra la informació extra de depuració"

#. TRANSLATORS: command line option
#: client/as-builder.c:93
msgid "Add a cache ID to each component"
msgstr "Afegeix un id. de memòria cau a cadascun dels components"

#. TRANSLATORS: command line option
#: client/as-builder.c:96
msgid "Include failed results in the output"
msgstr "Inclou els resultats fallits a la sortida"

#. TRANSLATORS: command line option
#: client/as-builder.c:99
msgid "Add HiDPI icons to the tarball"
msgstr "Afegeix les icones HiDPI als arxius tar"

#. TRANSLATORS: command line option
#: client/as-builder.c:102
msgid "Add encoded icons to the XML"
msgstr "Afegeix les icones codificades al XML"

#. TRANSLATORS: command line option
#: client/as-builder.c:105
msgid "Do not compress the icons into a tarball"
msgstr "No comprimeix les icones en un arxiu tar"

#. TRANSLATORS: command line option
#: client/as-builder.c:108
msgid "Set the logging directory"
msgstr "Estableix el directori d'enregistrament"

#. TRANSLATORS: command line option
#: client/as-builder.c:111
msgid "Set the packages directory"
msgstr "Estableix el directori dels paquets"

#. TRANSLATORS: command line option
#: client/as-builder.c:114
msgid "Set the temporary directory"
msgstr "Estableix el directori temporal"

#. TRANSLATORS: command line option
#: client/as-builder.c:117 client/as-compose.c:346
msgid "Set the output directory"
msgstr "Estableix el directori de la sortida"

#. TRANSLATORS: command line option
#: client/as-builder.c:120 client/as-compose.c:349
msgid "Set the icons directory"
msgstr "Estableix el directori de les icones"

#. TRANSLATORS: command line option
#: client/as-builder.c:123
msgid "Set the cache directory"
msgstr "Estableix el directori de la memòria cau"

#. TRANSLATORS: command line option
#: client/as-builder.c:126 client/as-compose.c:358
msgid "Set the basenames of the output files"
msgstr "Estableix la base dels noms per als fitxers de sortida"

#. TRANSLATORS: command line option
#: client/as-builder.c:129 client/as-compose.c:352
msgid "Set the origin name"
msgstr "Estableix el nom d'origen"

#. TRANSLATORS: command line option
#: client/as-builder.c:132
msgid "Set the number of threads"
msgstr "Estableix el nombre de fils d'execució"

#. TRANSLATORS: command line option
#: client/as-builder.c:135 client/as-compose.c:355
msgid "Set the minimum icon size in pixels"
msgstr "Estableix la mida màxima de la icona en píxels"

#. TRANSLATORS: command line option
#: client/as-builder.c:138
msgid "Set the old metadata location"
msgstr "Estableix la ubicació de les metadades antigues"

#. TRANSLATORS: command line option
#: client/as-builder.c:141
msgid "Ignore certain types of veto"
msgstr "No facis cas de certs tipus de vetos"

#. TRANSLATORS: error message
#: client/as-builder.c:155 client/as-compose.c:372 client/as-util.c:4432
msgid "Failed to parse arguments"
msgstr "No s'han pogut analitzar els arguments"

#. TRANSLATORS: error message
#: client/as-builder.c:259
msgid "Failed to set up builder"
msgstr "No s'ha pogut preparar el constructor"

#. TRANSLATORS: error message
#: client/as-builder.c:278
msgid "Failed to open packages"
msgstr "No s'han pogut obrir els paquets"

#. TRANSLATORS: information message
#: client/as-builder.c:288
msgid "Scanning packages..."
msgstr "S'estan escanejant els paquets..."

#. TRANSLATORS: error message
#: client/as-builder.c:298
msgid "Failed to add package"
msgstr "No s'ha pogut afegir el paquet"

#. TRANSLATORS: information message
#: client/as-builder.c:304
#, c-format
msgid "Parsed %u/%u files..."
msgstr "S'han analitzat %u/%u fitxers..."

#. TRANSLATORS: error message
#: client/as-builder.c:314
msgid "Failed to generate metadata"
msgstr "No s'han pogut generar les metadades"

#. TRANSLATORS: information message
#: client/as-builder.c:321 client/as-compose.c:507
msgid "Done!"
msgstr "Fet!"

#. TRANSLATORS: we've saving the icon file to disk
#: client/as-compose.c:124 client/as-compose.c:179
msgid "Saving icon"
msgstr "S'està desant la icona"

#. TRANSLATORS: command line option
#: client/as-compose.c:343
msgid "Set the prefix"
msgstr "Estableix el prefix"

#. TRANSLATORS: we're generating the AppStream data
#: client/as-compose.c:412
msgid "Processing application"
msgstr "S'està processant l'aplicació"

#. TRANSLATORS: the .appdata.xml file could not
#. * be loaded
#: client/as-compose.c:418
msgid "Error loading AppData file"
msgstr "S'ha produït un error en carregar el fitxer AppData"

#. TRANSLATORS: the .mo files could not be parsed
#: client/as-compose.c:431
msgid "Error parsing translations"
msgstr "S'ha produït un error en analitzar sintàcticament les transaccions"

#. TRANSLATORS: we could not auto-add the kudo
#: client/as-compose.c:442
msgid "Error parsing kudos"
msgstr "S'ha produït un error en analitzar sintàcticament kudos"

#. TRANSLATORS: we could not auto-add the provides
#: client/as-compose.c:453
msgid "Error parsing provides"
msgstr "S'ha produït un error en analitzar sintàcticament provides"

#. TRANSLATORS: the .desktop file could not
#. * be loaded
#: client/as-compose.c:469
msgid "Error loading desktop file"
msgstr "S'ha produït un error en carregar el fitxer d'escriptori"

#. TRANSLATORS: this is when the folder could
#. * not be created
#: client/as-compose.c:482
msgid "Error creating output directory"
msgstr "S'ha produït un error en crear el directori de sortida"

#. TRANSLATORS: we've saving the XML file to disk
#: client/as-compose.c:491
msgid "Saving AppStream"
msgstr "S'està desant l'AppStream"

#. TRANSLATORS: this is when the destination file
#. * cannot be saved for some reason
#: client/as-compose.c:501
msgid "Error saving AppStream file"
msgstr "S'ha produït un error en desar el fitxer d'AppStream"

#. TRANSLATORS: this is a command alias
#: client/as-util.c:104
#, c-format
msgid "Alias to %s"
msgstr "Àlies a %s"

#: client/as-util.c:186
msgid "Command not found, valid commands are:"
msgstr "No s'ha trobat l'ordre, les ordres vàlides són:"

#. TRANSLATORS: any manual changes required?
#. * also note: FIXME is a hardcoded string
#: client/as-util.c:478
msgid "Please review the file and fix any 'FIXME' items"
msgstr ""
"Si us plau, reviseu el fitxer i corregiu qualsevol dels elements «FIXME»"

#. TRANSLATORS: information message
#: client/as-util.c:496
msgid "Old API version"
msgstr "Versió antiga de l'API"

#. TRANSLATORS: information message
#: client/as-util.c:508
msgid "New API version"
msgstr "Nova versió de l'API"

#: client/as-util.c:527
msgid "Not enough arguments, expected old.xml new.xml version"
msgstr "No hi ha prou arguments, s'esperaven els fitxers antic.xml nou.xml"

#. TRANSLATORS: the %s and %s are file types,
#. * e.g. "appdata" to "appstream"
#: client/as-util.c:563
#, c-format
msgid "Conversion %s to %s is not implemented"
msgstr "La conversió de %s a %s no està implementada"

#: client/as-util.c:580
msgid "Not enough arguments, expected file.xml"
msgstr "No hi ha prou arguments, s'esperava el fitxer.xml"

#. TRANSLATORS: %s is a file type,
#. * e.g. 'appdata'
#: client/as-util.c:607
#, c-format
msgid "File format '%s' cannot be upgraded"
msgstr "No es pot actualitzar el format de fitxer «%s»"

#. TRANSLATORS: not a recognised file type
#: client/as-util.c:649 client/as-util.c:1047 client/as-util.c:1148
msgid "Format not recognised"
msgstr "Format no reconegut"

#. TRANSLATORS: probably wrong XML
#: client/as-util.c:1980
msgid "No desktop applications found"
msgstr "No s'han trobat aplicacions d'escriptori"

#. TRANSLATORS: the file is valid
#: client/as-util.c:2711
msgid "OK"
msgstr "CORRECTE"

#: client/as-util.c:2716
msgid "FAILED"
msgstr "FALLIT"

#: client/as-util.c:2812
msgid "Validation failed"
msgstr "Ha fallat la validació"

#: client/as-util.c:2856
msgid "Validation of files failed"
msgstr "Ha fallat la validació dels fitxers"

#. TRANSLATORS: application was removed
#: client/as-util.c:3879
msgid "Removed"
msgstr "Suprimit"

#. TRANSLATORS: application was added
#: client/as-util.c:3892
msgid "Added"
msgstr "Afegit"

#. TRANSLATORS: this is when a device ctrl+c's a watch
#: client/as-util.c:4141
msgid "Cancelled"
msgstr "Cancel·lat"

#. TRANSLATORS: this is the --nonet argument
#: client/as-util.c:4170
msgid "Do not use network access"
msgstr "No utilitzis l'accés a la xarxa"

#. TRANSLATORS: command line option
#: client/as-util.c:4176
msgid "Show version"
msgstr "Mostra la versió"

#. TRANSLATORS: command line option
#: client/as-util.c:4179
msgid "Enable profiling"
msgstr "Habilita el perfilament"

#. TRANSLATORS: command description
#: client/as-util.c:4209
msgid "Converts AppStream metadata from one version to another"
msgstr "Converteix les metadades AppStream d'una versió a una altra"

#. TRANSLATORS: command description
#: client/as-util.c:4215
msgid "Upgrade AppData metadata to the latest version"
msgstr ""
"Actualitza a nivell de llançament les metadades AppData a l'última versió"

#. TRANSLATORS: command description
#: client/as-util.c:4221
msgid "Creates an example Appdata file from a .desktop file"
msgstr "Crea un fitxer Appdata d'exemple des d'un fitxer .desktop"

#. TRANSLATORS: command description
#: client/as-util.c:4227
msgid "Dumps the applications in the AppStream metadata"
msgstr "Bolca les aplicacions a les metadades AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4233
msgid "Search for AppStream applications"
msgstr "Cerca aplicacions AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4239
msgid "Search for AppStream applications by package name"
msgstr "Cerca aplicacions AppStream pel nom del paquet"

#. TRANSLATORS: command description
#: client/as-util.c:4245
msgid "Show all installed AppStream applications"
msgstr "Mostra totes les aplicacions AppStream instal·lades"

#. TRANSLATORS: command description
#: client/as-util.c:4251
msgid "Search for AppStream applications by category name"
msgstr "Cerca aplicacions AppStream pel nom de la categoria"

#. TRANSLATORS: command description
#: client/as-util.c:4257
msgid "Display application search tokens"
msgstr "Mostra els marcadors de cerca de l'aplicació"

#. TRANSLATORS: command description
#: client/as-util.c:4263
msgid "Installs AppStream metadata"
msgstr "Instal·la les metadades AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4269
msgid "Installs AppStream metadata with new origin"
msgstr "Instal·la les metadades AppStream amb un origen nou"

#. TRANSLATORS: command description
#: client/as-util.c:4275
msgid "Uninstalls AppStream metadata"
msgstr "Desinstal·la les metadades AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4281
msgid "Create an HTML status page"
msgstr "Crea una pàgina HTML de l'estat"

#. TRANSLATORS: command description
#: client/as-util.c:4287
msgid "Create an CSV status document"
msgstr "Crea un document CSV de l'estat"

#. TRANSLATORS: command description
#: client/as-util.c:4293
msgid "Create an HTML matrix page"
msgstr "Crea una pàgina HTML amb la taula"

#. TRANSLATORS: command description
#: client/as-util.c:4299
msgid "List applications not backed by packages"
msgstr "Llista les aplicacions que no compten amb el suport dels paquets"

#. TRANSLATORS: command description
#: client/as-util.c:4305
msgid "Validate an AppData or AppStream file"
msgstr "Valida un fitxer AppData o AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4311
msgid "Validate an AppData or AppStream file (relaxed)"
msgstr "Valida un fitxer AppData o AppStream (relaxat)"

#. TRANSLATORS: command description
#: client/as-util.c:4317
msgid "Validate an AppData or AppStream file (strict)"
msgstr "Valida un fitxer AppData o AppStream (estricte)"

#. TRANSLATORS: command description
#: client/as-util.c:4323
msgid "Convert an AppData file to NEWS format"
msgstr "Converteix un fitxer AppData al format NEWS"

#. TRANSLATORS: command description
#: client/as-util.c:4329
msgid "Convert an NEWS file to AppData format"
msgstr "Converteix un fitxer NEWS al format AppData"

#. TRANSLATORS: command description
#: client/as-util.c:4335
msgid "Check installed application data"
msgstr "Comprova les dades de les aplicacions instal·lades"

#. TRANSLATORS: command description
#: client/as-util.c:4341
msgid "Replace screenshots in source file"
msgstr "Substitueix les captures de pantalles en un fitxer d'origen"

#. TRANSLATORS: command description
#: client/as-util.c:4347
msgid "Add a provide to a source file"
msgstr "Afegeix un proporciona a un fitxer d'origen"

#. TRANSLATORS: command description
#: client/as-util.c:4353
msgid "Add a language to a source file"
msgstr "Afegeix un idioma a un fitxer d'origen"

#. TRANSLATORS: command description
#: client/as-util.c:4359
msgid "Mirror upstream screenshots"
msgstr ""
"Crea una rèplica de les captures de pantalla de la línia de treball "
"principal"

#. TRANSLATORS: command description
#: client/as-util.c:4365
msgid "Mirror local firmware files"
msgstr "Crea una rèplica dels fitxers locals de microprogramari"

#. TRANSLATORS: command description
#: client/as-util.c:4371
msgid "Incorporate extra metadata from an external file"
msgstr "Incorpora dades extres des d'un fitxer extern"

#. TRANSLATORS: command description
#: client/as-util.c:4377
msgid "Compare the contents of two AppStream files"
msgstr "Compara el contingut de dos fitxers AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4383
msgid "Generate a GUID from an input string"
msgstr "Genera un GUID a partir d'una cadena de text"

#. TRANSLATORS: command description
#: client/as-util.c:4389
msgid "Modify an AppData file"
msgstr "Modifica un fitxer AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4395
msgid "Split an AppStream file to AppData and Metainfo files"
msgstr "Descompon un fitxer AppStream als fitxers AppData i Metainfo"

#. TRANSLATORS: command description
#: client/as-util.c:4401
msgid "Merge several files to an AppStream file"
msgstr "Combina diversos fitxers a un fitxer AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4407
msgid "Import a file to AppStream markup"
msgstr "Importa un fitxer al llenguatge de marques d'AppStream"

#. TRANSLATORS: command description
#: client/as-util.c:4413
msgid "Watch AppStream locations for changes"
msgstr "Observa les ubicacions AppStream per si hi ha canvis"

#. TRANSLATORS: program name
#: client/as-util.c:4426
msgid "AppStream Utility"
msgstr "Utilitat d'AppStream"

#: client/as-util.c:4450
msgid "Version:"
msgstr "Versió:"
